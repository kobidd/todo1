<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Todo;
use App\User;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\DB;
class TodoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
            $id = Auth::id();
            if (Gate::denies('manager')) {
                $boss = DB::table('employees')->where('employee',$id)->first();
                $id = $boss->manager;
            }
            $user = User::find($id);
            $todos = $user->todos;
            return view('todos.index', compact('todos'));
     
        //get all todos
        //$id=Auth::id();
        //$todos = Todo::all();
        //$todos = User::find($id)->todos;
       // return view('todos.index', ['todos'=> $todos]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Gate::denies('manager')) {
            abort(403,"Sorry you are not allowed to create todos..");
        } 
        return view ('todos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (Gate::denies('manager')) {
            abort(403,"Are you a hacker or what?");
        } 
        $todo = new Todo();
        $id=Auth::id();
        $todo->title = $request->title;
        $todo->user_id = $id;
        $todo->status = 0;
        $todo->save();
        return redirect('todos');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Gate::denies('manager')) {
            abort(403,"Are you a hacker or what?");
        } 
        $todo = Todo::find($id);
        return view('todos.edit', ['todo'=>$todo]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $todo = Todo::findOrFail($id);
        if (Gate::denies('manager')) {
            if ($request->has('title'))
                   abort(403,"You are not allowed to edit todos..");
        }    
        if(!$todo->user->id == Auth::id()) return(redirect('todos'));
        $todo->update($request->except(['_token']));
        if($request->ajax()){
            return Response::json(array('result'=>'success','status'=>$request->status),200);
        }
        return redirect('todos');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Gate::denies('manager')) {
            abort(403,"Are you a hacker or what?");
        } 
        $todo = Todo::find($id);
        if(!$todo->user->id == Auth::id()) return(redirect('todos'));
        $todo->delete();
        return redirect('todos');
    }
}