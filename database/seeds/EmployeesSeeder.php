<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
class EmployeesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(
        [
            [
                    'name' => 'Kobi Dadon',
                    'email' => 'kobi@gmail.com',
                    'password' => Hash::make('12345678'),
                    'role' => 'employee',
                    'created_at' => date('Y-m-d G:i:s'),
            ],
            [
                    'name' => 'akiva',
                    'email' => 'akiva@gmail.com',
                    'password' => Hash::make('12345678'),
                    'role' => 'employee',
                    'created_at' => date('Y-m-d G:i:s'),
            ],
            [
                    'name' => 'amir',
                    'email' => 'amir@gmail.com',
                    'password' => Hash::make('12345678'),
                    'role' => 'employee',
                    'created_at' => date('Y-m-d G:i:s'),
            ],
            
            [
                'name' => 'matan',
                'email' => 'matan@gmail.com',
                'password' => Hash::make('12345678'),
                'role' => 'employee',
                'created_at' => date('Y-m-d G:i:s'),
            ],
                ]);
    }
}
