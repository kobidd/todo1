@extends('layouts.app')
@section('content')
<h1>Create New todo</h1>
<form method = 'post' action="{{action('TodoController@store')}}">
{{csrf_field()}}
<div class = "form-group">
    <label for = "title">What would you like todo?</label>
    <input type= "text" class = "form-control" name= "title">
</div>

<div class = "form-group">
    <input type ="submit" class = "form-control" name="submit" value ="Just Do It">
</div>

</form>
@endsection