@extends('layouts.app')
@section('content')
<h1>Edit todo</h1>
<form method = 'post' action="{{action('TodoController@update', $todo->id)}}">
@csrf
@method('PATCH')
<div class = "form-group">
    <label for = "title">Todo to Update:</label>
    <input type= "text" class = "form-control" name= "title" value = "{{$todo->title}}">
</div>

<div class = "form-group">
    <input type ="submit" class = "form-control" name="submit" value ="Just Do It">
</div>

</form>

<h1>Delete todo</h1>
<form method = 'post' action="{{action('TodoController@destroy', $todo->id)}}">
@csrf
@method('DELETE')
<div class = "form-group">
    <input type ="submit" class = "form-control" name="submit" value ="Delete">
</div>

</form>
@endsection